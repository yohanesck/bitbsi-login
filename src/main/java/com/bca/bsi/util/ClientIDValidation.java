package com.bca.bsi.util;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

public class ClientIDValidation {
    private final JdbcTemplate jdbcTemplate;
    private final String hashcode;
    private final String clientID;

    public ClientIDValidation(JdbcTemplate jdbcTemplate, String hashcode, String clientID) {
        this.jdbcTemplate = jdbcTemplate;
        this.clientID = clientID;
        this.hashcode = hashcode;
    }

    public boolean isAllowed() {
        String result;
        try {
            Map<String, Object> inParamMap = new HashMap<String, Object>();
            inParamMap.put("P_CLIENT_ID", clientID);
            inParamMap.put("P_CLIENT_NAME", Constant.CLIENT_NAME);

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
            Map<String, Object> map = simpleJdbcCall.withoutProcedureColumnMetaDataAccess()
                    .withProcedureName("VALIDATE_CLIENT_ID")
                    .declareParameters(new SqlParameter[]{
                            new SqlInOutParameter("P_CLIENT_ID", Types.VARCHAR),
                            new SqlParameter("P_CLIENT_NAME", Types.VARCHAR),
                            new SqlOutParameter("P_RESULT", Types.VARCHAR),
                    })
                    .execute(inParamMap);

            result = (String) map.get("P_RESULT");

            if (result.equalsIgnoreCase("true")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
