package com.bca.bsi.util;

import org.springframework.http.HttpStatus;

public class ErrorMapping {
    // Error Code
    public static String SUCCESS_ERROR_CODE = String.valueOf(HttpStatus.OK.value());
    public static String NOT_FOUND = String.valueOf(HttpStatus.NOT_FOUND.value());
    public static String UNAUTHORIZED = String.valueOf(HttpStatus.UNAUTHORIZED.value());
    public static String FORBIDDEN = String.valueOf(HttpStatus.FORBIDDEN.value());
    public static String BAD_REQUEST = String.valueOf(HttpStatus.BAD_REQUEST.value());
    public static String GLOBAL_ERROR_CODE = String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value());

    // Error Message Indonesian
    public static String SUCCESS_ERROR_MESSAGE_INDONESIAN = "Berhasil";
    public static String GLOBAL_ERROR_MESSAGE_INDONESIAN = "Error Global";
    public static String NOT_FOUND_ERROR_MESSAGE_INDONESIAN = "Data Tidak Ditemukan";
    public static String FORBIDDEN_ERROR_MESSAGE_INDONESIAN = "Akses Tidak Tersedia";
    public static String BAD_REQUEST_ERROR_MESSAGE_INDONESIAN = "Request Body Tidak Valid";
    public static String WRONG_USERNAME_OR_PASSWORD_INDONESIAN = "BCA ID atau Password Salah";
}
