package com.bca.bsi.login.dao;

import com.bca.bsi.login.model.request.Login;
import com.bca.bsi.login.model.response.ResponseMessage;

public interface LoginDao {
    ResponseMessage doLogin(Login login, String clientID, String hashcode);
}
