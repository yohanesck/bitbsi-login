package com.bca.bsi.login.dao;

import com.bca.bsi.login.model.request.Login;
import com.bca.bsi.login.model.response.*;
import com.bca.bsi.util.ErrorMapping;
import com.bca.bsi.util.ErrorSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Repository
public class LoginDaoImpl implements LoginDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public ResponseMessage doLogin(Login login, String clientID, String hashcode) {
        InquiryWelmaUser inquiryWelmaUser = new InquiryWelmaUser();
        InquiryForumUser inquiryForumUser;
        ErrorSchema errorSchema = new ErrorSchema();
        OutputSchema outputSchema = new OutputSchema();
        ResponseMessage responseMessage = new ResponseMessage();
        String result;

        // Get hashcode
        if (hashcode == null || "".equals(hashcode.trim())) {
        }

        // Validate Client ID
//        if (! new ClientIDValidation(jdbcTemplate, hashcode, clientID).isAllowed()) {
//            errorSchema.setErrorCode(ErrorMapping.FORBIDDEN);
//            errorSchema.setErrorMessage(ErrorMapping.FORBIDDEN_ERROR_MESSAGE_INDONESIAN);
//
//            responseMessage.setErrorSchema(errorSchema);
//
//            return responseMessage;
//        }

        try {
            Map<String, Object> inParamMap = new HashMap<String, Object>();
            inParamMap.put("P_BCA_ID", login.getBcaId());
            inParamMap.put("P_PASSWORD", login.getPassword());

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
            Map<String, Object> map = simpleJdbcCall.withoutProcedureColumnMetaDataAccess()
                    .withProcedureName("DO_LOGIN_MOBILE_WELMA_APPS")
                    .declareParameters(new SqlParameter[]{
                            new SqlInOutParameter("P_BCA_ID", Types.VARCHAR),
                            new SqlParameter("P_PASSWORD", Types.VARCHAR),
                            new SqlOutParameter("P_RESULT", Types.VARCHAR),
                            new SqlOutParameter("P_NAME", Types.VARCHAR),
                            new SqlOutParameter("P_PROFILE_RISIKO", Types.NUMERIC),
                            new SqlOutParameter("P_EMAIL", Types.VARCHAR),
                            new SqlOutParameter("P_NOMOR_REKENING", Types.VARCHAR)
                    })
                    .execute(inParamMap);

            result = (String) map.get("P_RESULT");

            if (result.equalsIgnoreCase("true")) {
                errorSchema.setErrorMessage(ErrorMapping.SUCCESS_ERROR_MESSAGE_INDONESIAN);
                errorSchema.setErrorCode(ErrorMapping.SUCCESS_ERROR_CODE);

                inquiryWelmaUser.setBcaId((String) map.get("P_BCA_ID"));
                inquiryWelmaUser.setEmail((String) map.get("P_EMAIL"));
                inquiryWelmaUser.setName((String) map.get("P_NAME"));
                inquiryWelmaUser.setRekening((String) map.get("P_NOMOR_REKENING"));
                inquiryWelmaUser.setProfileRisiko(String.valueOf(map.get("P_PROFILE_RISIKO")));

                // Call inquiry forum user private function
                inquiryForumUser = inquiryForumUser(login.getBcaId());

                outputSchema.setInquiryWelmaUser(inquiryWelmaUser);
                outputSchema.setInquiryForumUser(inquiryForumUser);

                responseMessage.setErrorSchema(errorSchema);
                responseMessage.setOutputSchema(outputSchema);
            } else {
                errorSchema.setErrorCode(ErrorMapping.FORBIDDEN);
                errorSchema.setErrorMessage(ErrorMapping.WRONG_USERNAME_OR_PASSWORD_INDONESIAN);

                outputSchema.setInquiryWelmaUser(null);

                responseMessage.setErrorSchema(errorSchema);
            }
        } catch (Exception e) {
            errorSchema.setErrorCode(ErrorMapping.GLOBAL_ERROR_CODE);
            errorSchema.setErrorMessage(ErrorMapping.GLOBAL_ERROR_MESSAGE_INDONESIAN);

            outputSchema.setInquiryWelmaUser(null);

            responseMessage.setErrorSchema(errorSchema);

            e.printStackTrace();
        }

        return responseMessage;
    }

    private InquiryForumUser inquiryForumUser(String bcaId) {
        InquiryForumUser inquiryForumUser = new InquiryForumUser();
        String result;

        try {
            Map<String, Object> inParamMap = new HashMap<String, Object>();
            inParamMap.put("P_BCA_ID", bcaId);

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
            Map<String, Object> map = simpleJdbcCall.withoutProcedureColumnMetaDataAccess()
                    .withProcedureName("GET_FORUM_PROFILE")
                    .declareParameters(new SqlParameter[]{
                            new SqlInOutParameter("P_BCA_ID", Types.VARCHAR),
                            new SqlOutParameter("P_RESULT", Types.VARCHAR),
                            new SqlOutParameter("P_USERNAME", Types.VARCHAR),
                            new SqlOutParameter("P_FOLLOWER", Types.NUMERIC),
                            new SqlOutParameter("P_FOLLOWING", Types.NUMERIC),
                            new SqlOutParameter("P_IMG_PROFILE", Types.VARCHAR),
                            new SqlOutParameter("P_PROFILE_ID", Types.NUMERIC),
                    })
                    .execute(inParamMap);

            result = (String) map.get("P_RESULT");

            if (result.equalsIgnoreCase("true")) {
                inquiryForumUser.setBcaId(String.valueOf(map.get("P_BCA_ID")));
                inquiryForumUser.setFollower(String.valueOf(map.get("P_FOLLOWER")));
                inquiryForumUser.setFollowing(String.valueOf(map.get("P_FOLLOWING")));
                inquiryForumUser.setUsername((String) map.get("P_USERNAME"));
                inquiryForumUser.setImageUrl((String) map.get("P_IMG_PROFILE"));
                inquiryForumUser.setProfileId(String.valueOf(map.get("P_PROFILE_ID")));
            } else {
                inquiryForumUser = null;
            }
        } catch (Exception e) {
            inquiryForumUser = null;
            e.printStackTrace();
        }

        return inquiryForumUser;
    }
}
