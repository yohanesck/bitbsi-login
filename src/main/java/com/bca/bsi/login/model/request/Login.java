package com.bca.bsi.login.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Login {
    @JsonProperty("bca_id")
    private String bcaId;
    private String password;

    public String getBcaId() {
        return this.bcaId;
    }

    public void setBcaId(String bcaId) {
        this.bcaId = bcaId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
