package com.bca.bsi.login.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryForumUser {
    @JsonProperty("profile_id")
    private String profileId;

    @JsonProperty("bca_id")
    private String bcaId;

    @JsonProperty("username")
    private String username;

    @JsonProperty("follower_count")
    private String follower;

    @JsonProperty("following_count")
    private String following;

    @JsonProperty("img_profile")
    private String imageUrl;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getBcaId() {
        return bcaId;
    }

    public void setBcaId(String bcaId) {
        this.bcaId = bcaId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFollower() {
        return follower;
    }

    public void setFollower(String follower) {
        this.follower = follower;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
