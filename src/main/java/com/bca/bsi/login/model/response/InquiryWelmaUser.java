package com.bca.bsi.login.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryWelmaUser {
    private String name;
    private String email;

    @JsonProperty("bca_id")
    private String bcaId;

    @JsonProperty("nomor_rekening")
    private String rekening;

    @JsonProperty("profile_risiko")
    private String profileRisiko;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBcaId() {
        return bcaId;
    }

    public void setBcaId(String bcaId) {
        this.bcaId = bcaId;
    }

    public String getRekening() {
        return rekening;
    }

    public void setRekening(String rekening) {
        this.rekening = rekening;
    }

    public String getProfileRisiko() {
        return profileRisiko;
    }

    public void setProfileRisiko(String profileRisiko) {
        this.profileRisiko = profileRisiko;
    }
}
