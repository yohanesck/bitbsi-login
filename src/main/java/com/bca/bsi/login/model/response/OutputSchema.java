package com.bca.bsi.login.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OutputSchema {
    @JsonProperty("forum_user")
    private InquiryForumUser inquiryForumUser;

    @JsonProperty("welma_user")
    private InquiryWelmaUser inquiryWelmaUser;

    public InquiryForumUser getInquiryForumUser() {
        return inquiryForumUser;
    }

    public void setInquiryForumUser(InquiryForumUser inquiryForumUser) {
        this.inquiryForumUser = inquiryForumUser;
    }

    public InquiryWelmaUser getInquiryWelmaUser() {
        return inquiryWelmaUser;
    }

    public void setInquiryWelmaUser(InquiryWelmaUser inquiryWelmaUser) {
        this.inquiryWelmaUser = inquiryWelmaUser;
    }
}
