package com.bca.bsi.login.services;

import com.bca.bsi.login.dao.LoginDao;
import com.bca.bsi.login.model.request.Login;
import com.bca.bsi.login.model.response.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Resource
public class LoginServiceImpl implements LoginService {
    @Autowired
    LoginDao loginDao;

    @Override
    public ResponseMessage doLogin(Login login, String clientID, String hashcode) {
        return loginDao.doLogin(login, clientID, hashcode);
    }

}
