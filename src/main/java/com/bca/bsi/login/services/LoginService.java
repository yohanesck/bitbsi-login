package com.bca.bsi.login.services;

import com.bca.bsi.login.model.request.Login;
import com.bca.bsi.login.model.response.ResponseMessage;

public interface LoginService {
    ResponseMessage doLogin(Login login, String clientID, String hashcode);
}
