package com.bca.bsi.login.controller;

import com.bca.bsi.login.model.request.Login;
import com.bca.bsi.login.model.response.ResponseMessage;
import com.bca.bsi.login.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.core.annotation.AliasFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.HeaderParam;

@CrossOrigin(origins = "*")
@RestController
public class LoginController {
    @Autowired
    LoginService loginService;

//    @PostMapping(value = "/mobile/login", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseMessage> inquiryLogin(@RequestBody Login login,
                                                        @RequestHeader(value = "client-id") String clientID,
                                                        @RequestHeader(value = "hashcode") String hashcode) {
        ResponseMessage result = this.loginService.doLogin(login, clientID, hashcode);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}


